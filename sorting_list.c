//
// Created by Mykhailo Frankevich on 4/26/17.
//

#include "ft_ls.h"

static void _swap_nodes_content(struct s_ls_list *first, struct s_ls_list *second)
{
	struct stat			*tmp_st;
	char				*tmp_name;

	tmp_st = first->st;
	tmp_name = first->folder_name;
	first->st = second->st;
	first->folder_name = second->folder_name;
	second->st = tmp_st;
	second->folder_name = tmp_name;
	return ;
}

static int _cmp_two_nodes(struct s_ls_list *first, struct s_ls_list *second)
{
	return (ft_strcmp(first->folder_name, second->folder_name));
}

static struct s_ls_list *_partition(struct s_ls_list *head, struct s_ls_list *tail)
{
	struct s_ls_list *tmp;
	struct s_ls_list *cur;

	tmp = tail;
	cur = head;
	while (cur != tail)
	{
		if (_cmp_two_nodes(tmp, cur) < 0)
		{
			_swap_nodes_content(tmp, cur);
			tmp = (tmp ? tail : tmp->prev);
		}
		cur = cur->next;
	}
	tmp = (tmp ? tail : tmp->prev);
	_swap_nodes_content(tmp, cur);
	return (tmp);
}

static void		_quicksort(struct s_ls_list *head, struct s_ls_list *tail)
{
	struct s_ls_list *pivot;
	if (head != NULL && head != tail && head != tail->next)
	{
		pivot = _partition(head, tail);
		_quicksort(head, pivot->prev);
		_quicksort(pivot->next, tail);
	}
}

void		quicksort_list(struct s_ls_list *head)
{
	struct s_ls_list *crawler;

	crawler = head;
	while (crawler->next)
		crawler = crawler->next;
	_quicksort(head, crawler);
	return ;
}
