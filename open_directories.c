# include "ft_ls.h"

static char *folder_name_cat(char *folder, char *cat_folder)
{
	char *output;
	char *tmp;

	output = (char*)malloc(ft_strlen(folder) + ft_strlen(cat_folder) + 2);
	tmp = output;
	while (*folder)
		*tmp++ = *folder++;
	*tmp++ = '/';
	while (*cat_folder)
		*tmp++ = *cat_folder++;
	*tmp = '\0';
	return(output);
}

void			print_folders_out_of_list(t_list *lst, char *folder, struct s_args *args)
{
	t_list *crawler;
	t_list *prev;
	char *tmp;

	crawler = lst;
	while (crawler)
	{
		tmp = folder_name_cat(folder, crawler->content);
		print_folder_content(args, tmp);
		free(tmp);
		prev = crawler;
		crawler = crawler->next;
		free(prev->content);
		free(prev);
	}
}

void		print_sorted_folders(struct s_folder fold)
{
	struct s_ls_list *crawler;
	struct s_ls_list *prev;

	if (fold.print == NULL)
		return ;
	quicksort_list(fold.print);
	crawler = fold.print;
	while (crawler)
	{
		ft_putstr(crawler->folder_name);
		ft_putchar('\n');
		prev = crawler;
		crawler = crawler->next;
		free(prev->folder_name);
		if (prev->st)
			free(prev->st);
		free(prev);
	}
	return ;
}

void		print_folder_content(struct s_args *args, char *folder)
{
	struct s_folder	fold;
	struct stat		*st;
	char 			*cur_folder;

	ft_bzero(&fold, sizeof(fold));
	fold.dir = opendir(folder);
	if (fold.dir == NULL)
	{
		print_directory_not_found(folder);
		return ;
	}
	if (ft_strcmp(folder, ".") && ft_strcmp(folder, ".."))
	{
		print_directory_title(folder);
	}
	while ((fold.dirnt = readdir(fold.dir)) != NULL)
	{
		if ((fold.dirnt->d_name[0] != '.' || args->flag_a))
		{
			cur_folder = folder_name_cat(folder, fold.dirnt->d_name);
			lstat(cur_folder, st);
			free(cur_folder);
			push_struct_to_list(&fold.print, fold.dirnt, st);
//			print_dat_data(folder, args, &fold);
			if ((st->st_mode & S_IFMT) == S_IFDIR && args->flag_big_r && ft_strcmp(fold.dirnt->d_name, ".") && ft_strcmp(fold.dirnt->d_name, ".."))
				ft_lstpushback(&fold.lst ,ft_lstnew(fold.dirnt->d_name, ft_strlen(fold.dirnt->d_name) + 1));
		}
	}
	closedir(fold.dir);
	print_sorted_folders(fold);
	ft_putchar('\n');
	if (fold.lst && args->flag_big_r)
	{
		print_folders_out_of_list(fold.lst, folder, args);
//		ft_lstdel(&fold.lst, &free_bzero);
	}
	return ;
}

void	we_are_going_to_do_stuff(struct s_args *args)
{
	t_list	*crawler;

	if (args->list_fold == NULL)
	{
		ft_lstadd(&(args->list_fold), ft_lstnew(".\0", 2));
	}
	crawler = args->list_fold;
	while (crawler)
	{
		print_folder_content(args, crawler->content);
		crawler = crawler->next;
	}
	sleep(999);
}

/* (st.st_mode & S_IFMT) == S_IFDIR */

/*  struct stat fileStat;
    if(stat(argv[1],&fileStat) < 0)
        return 1;

    printf("Information for %s\n",argv[1]);
    printf("---------------------------\n");
    printf("File Size: \t\t%d bytes\n",fileStat.st_size);
    printf("Number of Links: \t%d\n",fileStat.st_nlink);
    printf("File inode: \t\t%d\n",fileStat.st_ino);

    printf("File Permissions: \t");
    printf( (S_ISDIR(fileStat.st_mode)) ? "d" : "-");
    printf( (fileStat.st_mode & S_IRUSR) ? "r" : "-");
    printf( (fileStat.st_mode & S_IWUSR) ? "w" : "-");
    printf( (fileStat.st_mode & S_IXUSR) ? "x" : "-");
    printf( (fileStat.st_mode & S_IRGRP) ? "r" : "-");
    printf( (fileStat.st_mode & S_IWGRP) ? "w" : "-");
    printf( (fileStat.st_mode & S_IXGRP) ? "x" : "-");
    printf( (fileStat.st_mode & S_IROTH) ? "r" : "-");
    printf( (fileStat.st_mode & S_IWOTH) ? "w" : "-");
    printf( (fileStat.st_mode & S_IXOTH) ? "x" : "-");
    printf("\n\n");

    printf("The file %s a symbolic link\n", (S_ISLNK(fileStat.st_mode)) ? "is" : "is not");
*/

/*
 *        struct stat st;
       ...

      switch (st.st_mode & S_IFMT) {
        case S_IFREG:
            puts("|| regular file");
            break;
        case S_IFDIR:
            puts("|| directory");
            break;
        case S_IFCHR:
            puts("|| character device");
            break;
        case S_IFBLK:
            puts("|| block device");
            break;
        case S_IFLNK:
            puts("|| symbolic link");
            break;
        case S_IFIFO:
            puts("|| pipe");
            break;
        case S_IFSOCK:
            puts("|| socket");
            break;
        default:
            puts("|| unknown");
     }
 */
