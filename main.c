//
// Created by Mykhailo Frankevich on 4/19/17.
//

#include "ft_ls.h"

int		main(int argc, char **argv)
{
	struct s_args args;

	ft_bzero(&args, sizeof(args));
	parsing_flags(argc, argv, &args);
	we_are_going_to_do_stuff(&args);
	ft_lstdel(&(args.list_fold), &free_bzero);
}