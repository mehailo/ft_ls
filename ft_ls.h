
#ifndef FT_LS_H
# define FT_LS_H

# include "libft/libft.h"
# include <dirent.h>
# include <sys/stat.h>
# include <pwd.h>
# include <grp.h>
# include <sys/types.h>
# include <sys/xattr.h>
# include <time.h>
# include <stdio.h>
# include <string.h>

struct s_args
{
	unsigned char	flag_r;
	unsigned char	flag_big_r;
	unsigned char	flag_l;
	unsigned char	flag_a;
	unsigned char	flag_t;
	char			**folders;
	t_list			*list_fold;
};

struct s_ls_list
{
	struct stat			*st;
	char				*folder_name;
	struct s_ls_list	*next;
	struct s_ls_list	*prev;
};

struct s_folder
{
	DIR					*dir;
	struct dirent		*dirnt;
	t_list				*lst;
	struct s_ls_list	*print;
};

void		parsing_flags(int argc, char **argv, struct s_args *args);
void		we_are_going_to_do_stuff(struct s_args *args);
void		print_folder_content(struct s_args *args, char *folder);
void		push_struct_to_list(struct s_ls_list **print, struct dirent *dir, struct stat *st);
void		quicksort_list(struct s_ls_list *head);
void		free_bzero(void *ptr, size_t size);
void		print_directory_title(char *folder);
void		print_directory_not_found(char *folder);

#endif
