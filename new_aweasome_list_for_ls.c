
#include "ft_ls.h"

void		push_struct_to_list(struct s_ls_list **print, struct dirent *dir, struct stat *st)
{
	struct s_ls_list	*elem;
	struct s_ls_list	*crawler;
	struct stat			*cp_st;
	char				*folder_name;

	elem = (struct s_ls_list*)ft_memalloc(sizeof(struct s_ls_list));
	cp_st = (struct stat*)malloc(sizeof(struct stat));
	folder_name = ft_strdup(dir->d_name);
	ft_memmove(cp_st , st, sizeof(*st));
	elem->st = cp_st;
	elem->folder_name = folder_name;
	elem->next = NULL;
	elem->prev = NULL;
	if (*print == NULL)
		*print = elem;
	else
	{
		crawler = *print;
		while (crawler->next)
			crawler = crawler->next;
		crawler->next = elem;
		elem->prev = crawler;
	}
	return ;
}
