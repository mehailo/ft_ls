#include "ft_ls.h"

static void		get_the_flags(char *argv, struct s_args *args)
{
	while (*argv)
	{
		argv++;
		if (*argv == 'R')
			args->flag_big_r = 1;
		else if (*argv == 'r')
			args->flag_r = 1;
		else if (*argv == 'l')
			args->flag_l = 1;
		else if (*argv == 'a')
			args->flag_a = 1;
		else if (*argv == 't')
			args->flag_t = 1;
	}
}

static void		get_the_folder(char *argv, struct s_args *args)
{
	int i;

	i = 0;
	while (args->folders[i])
		i++;
	args->folders[i] = ft_strdup(argv);
}

void			parsing_flags(int argc, char **argv, struct s_args *args)
{
	int i;

	i = 0;
	args->folders = (char**)malloc(sizeof(char*) * (argc - 1));
	while (i < argc - 1)
	{
		args->folders[i++] = NULL;
	}
	i = 1;
	while (i < argc)
	{
		if (argv[i][0] == '-')
			get_the_flags(argv[i], args);
		else
		{
			ft_lstadd(&(args->list_fold), ft_lstnew(argv[i], ft_strlen(argv[i]) + 1));
		}
		i++;
	}
}
