//
// Created by Mykhailo Frankevich on 4/26/17.
//
#include "ft_ls.h"

void	free_bzero(void *ptr, size_t size)
{
	ft_bzero(ptr, size);
	free(ptr);
	return ;
}

void	print_directory_title(char *folder)
{
	ft_putstr(folder);
	ft_putstr(":\n");
}

void	print_directory_not_found(char *folder)
{
	ft_putstr("ft_ls: ");
	ft_putstr(folder);
	ft_putstr(": No such file or directory\n");
}
